import { createApp } from 'vue'
import App from './App.vue'
import './style.css'
import "./plugin";

createApp(App).mount('#app')
